package com.shankar.myapp;

import com.shankar.calcengine.Adder;
import com.shankar.calcengine.CalculateBase;
import com.shankar.calcengine.CalculateHelper;
import com.shankar.calcengine.Divider;
import com.shankar.calcengine.DynamicHelper;
import com.shankar.calcengine.InvalidStatementException;
import com.shankar.calcengine.MathEquation;
import com.shankar.calcengine.MathProcessing;
import com.shankar.calcengine.Multiplier;
import com.shankar.calcengine.PowerOf;
import com.shankar.calcengine.Subtracter;

public class Main {
    public static void main(String[] args) {
//        useMathEquation();
        String[] statements = {
                "add 34.0d 33.2d",
                "power 5.0 2.0"
        };
        DynamicHelper helper = new DynamicHelper(new MathProcessing[]{
                new Adder(),
                new PowerOf()
        });
        for (String statement : statements) {
            String output = helper.process(statement);
            System.out.println(output);
        }
    }

    public static void useCalculateHelper() {
        String[] statements = {
                "add 1.0",
                "add xx 25.2",
                "addX 2.2 3.3",
                "divide 100.0d 50.0d",
                "add 34.0d 33.2d"
        };
        CalculateHelper helper = new CalculateHelper();
        for (String statement : statements) {
            try {
                helper.process(statement);
                System.out.println(helper);
            } catch (InvalidStatementException e) {
                System.out.println(e.getMessage());
                if (e.getCause() != null) {
                    System.out.println("Original exception: " + e.getCause().getMessage());
                }
            }
        }
    }

    public static MathEquation create(double leftVal, double rightVal, char opCode) {
        MathEquation equation = new MathEquation();
        equation.setLeftVal(leftVal);
        equation.setRightVal(rightVal);
        equation.setOpCode(opCode);
        return equation;
    }

    public void useMathEquation() {
        MathEquation[] equations = new MathEquation[4];
        equations[0] = create(100.0d, 50.0d, 'd');
        equations[1] = create(120.0d, 20.0d, 'd');
        equations[2] = new MathEquation(140.0d, 15.0d, 'd');
        equations[3] = new MathEquation(90.0d, 4.0d, 'd');

        for (MathEquation equation : equations) {
            equation.execute();
            System.out.println(equation.getResult());
        }


        System.out.println("\nUsing overloading:\n");
        double leftDouble = 10.0d;
        double rightDouble = 2.5d;
        int leftInt = 10;
        int rightInt = 3;
        MathEquation equationOverload = new MathEquation('d');
        equationOverload.execute(leftDouble, rightDouble);
        System.out.println("result = " + equationOverload.getResult());
        System.out.println("Using integers : \n");
        equationOverload.execute(leftInt, rightInt);
        System.out.println(equationOverload.getResult());

        System.out.println("\nUsing Inheritance : \n");
        CalculateBase[] calculateBases = {
                new Divider(100.0d, 50.0d),
                new Adder(25.0d, 92.0d),
                new Subtracter(28.0d, 92.0d),
                new Multiplier(99.0d, 92.0d)
        };
        for (CalculateBase calculator :
                calculateBases) {
            calculator.calculate();
            System.out.println("result = ");
            System.out.println(calculator.getResult());
        }
    }
}
