package com.shankar.calcengine;

enum MathCommand {
    Add,
    Subtract,
    Multiply,
    Divide
}
