public class Passenger implements Comparable {
    public static class RewardProgram {
        private int memberLevel;

        public int getMemberLevel() {
            return memberLevel;
        }

        public void setMemberLevel(int memberLevel) {
            this.memberLevel = memberLevel;
        }
    }
    RewardProgram rewardProgram1 = new RewardProgram();
    RewardProgram rewardProgram2 = new RewardProgram();

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    public static void main(String[] args) {
        Passenger passenger = new Passenger();
        passenger.rewardProgram1.setMemberLevel(2);
        passenger.rewardProgram2.setMemberLevel(4);
        System.out.println(passenger.rewardProgram1.getMemberLevel());
        System.out.println(passenger.rewardProgram2.getMemberLevel());
        System.out.println(passenger.rewardProgram1.getMemberLevel());
    }
}
