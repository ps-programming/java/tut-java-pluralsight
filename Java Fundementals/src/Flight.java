public class Flight {
    private String name;

    public Flight() {
    }

    public Flight(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Flight flight = new Flight("ak47");

//        FlightCrew flightCrew = new FlightCrew("hi");
//        FlightCrew flightCrew1 = new FlightCrew("hello");
    }
    class FlightCrew{
        private String crewName;

        public FlightCrew() {
        }

        public FlightCrew(String crewName) {
            this.crewName = crewName;
        }

        public String getCrewName() {
            return crewName;
        }

        public void setCrewName(String crewName) {
            this.crewName = crewName;
        }
    }
}
