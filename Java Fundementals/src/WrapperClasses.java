public class WrapperClasses {
    public static void main(String[] args) {
        Integer a = 100;
        int b = a;
        Integer c = b;
        Integer d = Integer.valueOf(100);
        int e = d.intValue();

        Float g = Float.valueOf(13.43f);
        float h = g.floatValue();
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        Character i = '9';
        System.out.println(Character.isDigit(i));
    }
}
